from astropy.wcs import wcs as _wcs
from astropy.coordinates import SkyCoord as _SkyCoord
import astropy.units as _u
from astropy.wcs.utils import skycoord_to_pixel as _skycoord_to_pixel


def from_ra_dec_to_x_y(wcs: _wcs.WCS, ra, dec):
    spatial_wcs = wcs.dropaxis(2)
    coords = _SkyCoord(ra=ra * _u.deg, dec=dec * _u.deg)
    return _skycoord_to_pixel(coords, spatial_wcs)
