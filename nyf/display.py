import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
from .coords import from_ra_dec_to_x_y


class CubeVisualizer:
    def __init__(self, cube, wcs=None, title=''):
        self.cube_shape = cube.shape
        self.wcs = wcs
        self.data = cube
        self.fig, self.ax = plt.subplots()
        self.ax.set_title(title)
        plt.subplots_adjust(left=0.25, bottom=0.25)
        self.axfreq = plt.axes([0.25, 0.1, 0.65, 0.03])
        resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
        button = Button(resetax, 'Reset', hovercolor='0.975')
        button.on_clicked(self.reset)
        self.sfreq = Slider(self.axfreq, 'Freq', 0.1, self.cube_shape[0] - 1, valinit=0, valstep=1)
        self.sfreq.on_changed(self.update)
        self.max_v = np.max(self.data)
        self.min_v = np.min(self.data)

        self.img = self.ax.imshow(self.data[0, :, :], vmin=self.min_v, vmax=self.max_v)

    def display_sources_catalog(self, catalog):
        if self.wcs is None:
            raise Exception("please define wcs property")
        x, y = from_ra_dec_to_x_y(self.wcs, catalog.ra, catalog.dec)

        self.ax.scatter(x, y, s=30, facecolors='none', edgecolors='r')

    def reset(self, event):
        self.sfreq.reset()
        self.update(0)

    def update(self, val):
        freq = val
        self.img.set_data(self.data[int(freq), :, :])
        self.fig.canvas.draw_idle()
