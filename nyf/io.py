import astropy.io.fits as _fits
import numpy as _np
import pandas as _pd
import os


def read_cube(file_path):
    hdus = _fits.open(file_path, memmap=True, lazy_load_hdus=True)
    hdu = hdus[0]
    hdu.data = _np.array(hdu.data, dtype='<f4')
    return hdu


def read_catalog(file_path):
    return _pd.read_csv(file_path, sep=' ')


def write_catalog(catalog: _pd.DataFrame, save_path):
    catalog.to_csv(save_path, sep=' ')