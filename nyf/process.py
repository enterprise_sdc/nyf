import numpy as _np
import numba as _nb

_nb.config.THREADING_LAYER = 'threadsafe'


def progressBar(iterable, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    total = len(iterable)

    # Progress Bar Printing Function
    def printProgressBar(iteration):
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + '-' * (length - filledLength)
        print(f'\r{prefix} |{bar}| {percent}% {suffix}', end=printEnd)

    # Initial Call
    printProgressBar(0)
    # Update Progress Bar
    for i, item in enumerate(iterable):
        yield item
        printProgressBar(i + 1)
    # Print New Line on Complete
    print()


def _cut_pad(image, pad_size):
    return image[pad_size:-pad_size, pad_size:-pad_size]


def compute_rms_map(data, filter_size):
    padded_data = _np.pad(data, filter_size, mode='reflect')
    rms_padded = compute_rms_cube_map(padded_data, filter_size)
    return _cut_pad(rms_padded, filter_size)


def compute_rms_cube(data, filter_size):
    output = _np.zeros_like(data)
    print('Allocated output matrix')
    for i in progressBar(range(data.shape[0]), prefix='computing rms map'):
        output[i, :, :] = compute_rms_map(data[i, :, :], filter_size)
    return output


@_nb.njit(parallel=True)
def compute_mean_cube_map(data, filter_size):
    stencil = _nb.stencil(
        lambda x, filter_size: _np.nanmean(x[-filter_size:filter_size, -filter_size:filter_size]),
        neighborhood=((-filter_size, filter_size), (-filter_size, filter_size))
    )
    _data = stencil(data, filter_size)
    return _data


@_nb.njit(parallel=True)
def compute_rms_cube_map(data, filter_size):
    stencil = _nb.stencil(
        lambda x, filter_size: _np.nanstd(x[-filter_size:filter_size, -filter_size:filter_size]),
        neighborhood=((-filter_size, filter_size), (-filter_size, filter_size))
    )
    _data = stencil(data, filter_size)
    return _data


from numba import cuda
import math


@cuda.jit
def average_gpu(x, out, filter_size):
    i, j = cuda.grid(2)
    f, n, m = x.shape
    if 0 <= i < n - 1 and 0 <= j < m - 1:
        for l in range(f):
            out[l, i, j] = 0.
            mean = 0
            std = 0
            for k in range(filter_size):
                for s in range(filter_size):
                    mean += x[l, i - filter_size // 2 + k, j - filter_size // 2 + s]
            mean /= filter_size * filter_size

            for k in range(filter_size):
                for s in range(filter_size):
                    std += (mean - x[l, i - filter_size // 2 + k, j - filter_size // 2 + s]) ** 2.
            std /= filter_size * filter_size
            out[l, i, j] = math.sqrt(std)


import math


def compute_rms_cube_map_cuda(data, filter_size):
    padded_data = _np.pad(data, [(0, 0),
                                 (filter_size, filter_size),
                                 (filter_size, filter_size)], mode='reflect')

    x_gpu = cuda.to_device(padded_data)
    output_data = cuda.device_array_like(padded_data)

    # I copied the four lines below from the Numba docs
    threadsperblock = (16, 16)
    blockspergrid_x = math.ceil(x_gpu.shape[1] / threadsperblock[0])
    blockspergrid_y = math.ceil(x_gpu.shape[2] / threadsperblock[1])
    blockspergrid = (blockspergrid_x, blockspergrid_y)
    average_gpu[blockspergrid, threadsperblock](x_gpu, output_data, filter_size)
    return _cut_pad(output_data, filter_size)


@cuda.jit
def freq_averaging_kernel(x, out, freq_filter_size):
    nu, i, j = cuda.grid(3)
    f, n, m = x.shape
    if 0 <= i < n - 1 and 0 <= j < m - 1 and freq_filter_size <= nu < f - freq_filter_size:
        for ii in range(-freq_filter_size, freq_filter_size):
            out[nu, i, j] += x[nu + ii, i, j]


def freq_averaging(data: _np.ndarray, time_filter_size):
    x_gpu = cuda.to_device(_np.ascontiguousarray(data))
    output_data = cuda.device_array_like(data)

    # I copied the four lines below from the Numba docs

    threadsperblock = (2, 16, 16)
    blockspergrid_nu = math.ceil(x_gpu.shape[0] / threadsperblock[0])
    blockspergrid_x = math.ceil(x_gpu.shape[1] / threadsperblock[1])
    blockspergrid_y = math.ceil(x_gpu.shape[2] / threadsperblock[2])

    blockspergrid = (blockspergrid_nu, blockspergrid_x, blockspergrid_y)
    freq_averaging_kernel[blockspergrid, threadsperblock](x_gpu, output_data, time_filter_size)
    return output_data


print("CUDA AVAILABLE: ", _nb.cuda.is_available())
_nb.cuda.detect()


def running_sum_cube_data(data, filter_size):
    output = _np.zeros_like(data)
    print('Allocated output matrix')
    block_size = filter_size
    F, X, Y = data.shape
    for i in progressBar(range(X), prefix='computing sum map'):
        for j in range(Y):
            output[:, i, j] = _np.convolve(data[:, i, j], _np.ones(filter_size), 'same') / filter_size
    return output


def compute_rms_cube_cuda(data, filter_size):
    output = _np.zeros_like(data)
    print('Allocated output matrix')
    block_size = 10
    for i in progressBar(range(data.shape[0] // block_size), prefix='computing rms map'):
        output[i * block_size: (i + 1) * block_size, :, :] = compute_rms_cube_map_cuda(
            data[i * block_size: (i + 1) * block_size, :, :], filter_size)
    return output


def freq_averaging_cuda(data, freq_filter_size):
    output = _np.zeros_like(data)
    print('Allocated output matrix')
    block_size = 10
    for i in progressBar(range(data.shape[1] // block_size), prefix='computing avg space time'):
        for j in range(data.shape[2] // block_size):
            output[:, i * block_size: (i + 1) * block_size, j * block_size: (j + 1) * block_size] = freq_averaging(
                data[:, i * block_size: (i + 1) * block_size, j * block_size: (j + 1) * block_size], freq_filter_size)
    return output
