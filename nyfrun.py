#!/usr/bin/env python3
from argparse import ArgumentParser
from nyf.io import read_cube, read_catalog
from nyf.process import compute_rms_cube, compute_rms_cube_cuda, running_sum_cube_data, freq_averaging_cuda
from nyf.display import CubeVisualizer
import matplotlib.pyplot as plt
import numpy as _np
import astropy.io.fits as _fits
import logging
import time


def parse_arguments():
    parser = ArgumentParser(description="Neutral hYdrogen source Finder")
    parser.add_argument('data_cube', help='Data cube path')
    parser.add_argument('--verbose', action='store_true')
    parser.add_argument('--action', choices=['display', 'subdivide'])
    parser.add_argument('--n_subdomains', help='number of subdomains it should be a perfect square', type=int)
    parser.add_argument('--working_path', help='working path')
    parser.add_argument('--catalog_path', help='catalog path', default=None)
    parser.add_argument('--base_name', help='base name path')

    return parser.parse_args()


def timeit(func, *args, **kwargs):
    start = time.process_time()
    return_value = func(*args, **kwargs)
    end = time.process_time()
    return end - start, return_value


def setup_logging(verbose=False):
    if verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO
    logging.basicConfig(level=level, format='%(asctime)-15s %(levelname)s %(thread)d %(funcName)s: %(message)s')


def compute_data_subdomain(data: _np.ndarray, n_subdomains, number_of_domain):
    _, xdim, ydim = data.shape
    linear_division = _np.floor(_np.sqrt(n_subdomains))
    x_new = int(xdim // linear_division)
    y_new = int(ydim // linear_division)
    x_index = int(number_of_domain % linear_division)
    y_index = int(number_of_domain // linear_division)
    xmin, xmax = x_new * x_index, x_new * (x_index + 1),
    ymin, ymax = y_new * y_index, y_new * (y_index + 1),
    return data[:, xmin: xmax,
           ymin: ymax], (xmin, xmax, ymin, ymax)


import os
import astropy.wcs as _wcs


def divide_input_cube_in_subdomains(cube: _fits.ImageHDU, number_of_domains, base_file_name, base_path):
    os.makedirs(base_path, exist_ok=True)
    for i in range(number_of_domains):
        logging.info('Extracting cube number %d of %d', i + 1, number_of_domains)
        full_path = os.path.join(base_path, f'{base_file_name}_{i:03d}.fits')
        wcs = _wcs.WCS(cube.header)
        sub_domain_data, slice = compute_data_subdomain(cube.data, number_of_domains, i)
        sub_domain_header = cube.header.copy()
        sub_domain_wcs = wcs[:, slice[0]: slice[1], slice[2]: slice[3]]
        sub_domain_header.update(sub_domain_wcs.to_header())
        _fits.writeto(full_path, header=sub_domain_header, data=sub_domain_data, overwrite=True)
        logging.info('Cube number %d of %d extracted', i + 1, number_of_domains)


def subdivide(cube_path, number_of_domains, base_file_name, base_path):
    cube = read_cube(cube_path)
    divide_input_cube_in_subdomains(cube, number_of_domains, base_file_name, base_path)


def display(cube_path, catalog_path):
    cube = read_cube(cube_path)

    tcude, out_cube_cuda = timeit(compute_rms_cube, cube.data, 20)
    visualizer = CubeVisualizer(out_cube_cuda, _wcs.WCS(cube.header))

    tcude, out_cube_cuda = timeit(freq_averaging_cuda, cube.data, 20)
    print('computation time is ', tcude)
    #tcude, out_cube_cuda = timeit(running_sum_cube_data, cube.data, 500)

    visualizer = CubeVisualizer(out_cube_cuda, _wcs.WCS(cube.header))
    if catalog_path:
        catalog = read_catalog(catalog_path)
        visualizer.display_sources_catalog(catalog)
    plt.show()


def compute(cube_path):
    cube = read_cube(cube_path)
    print('loaded')
    before = CubeVisualizer(cube.data, title='before')
    plt.imshow(cube.data[0, :, :])

    tcude, out_cube_cuda = timeit(compute_rms_cube_cuda, cube.data, 5)
    print('GPU time spent with rms', tcude)

    t, out_cube = timeit(compute_rms_cube, cube.data, 5)
    print('CPU time spent with rms', t)

    after = CubeVisualizer(out_cube_cuda, title='after')

    after = CubeVisualizer(out_cube - out_cube_cuda, title='difference')
    print('computed mean')

    plt.show()


def main():
    args = parse_arguments()
    setup_logging(args.verbose)
    if args.action == 'display':
        display(args.data_cube, args.catalog_path)
    elif args.action == 'subdivide':
        subdivide(args.data_cube, args.n_subdomains, args.base_name, args.working_path)


if __name__ == '__main__':
    main()
